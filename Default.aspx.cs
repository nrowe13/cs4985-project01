﻿using System.Web.UI;

/// <summary>
/// Summary description for CustomerList
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class Default : Page
{

    /// <summary>
    /// Handles the Click event of the btnCustomerDisplay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnCustomerDisplay_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("CustomerList.aspx");
    }
    /// <summary>
    /// Handles the Click event of the btnCustomerFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnCustomerFeedback_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }
}