﻿<%@ Page Title="Feedback Complete" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Feedback completed</h2>
    <asp:Label ID="lblFeedbackResponse" runat="server" Text="Your feedback has been recieved. Thank you for your input."></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblContactResponse" runat="server" Text=""></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnCustomerFeedback" runat="server" Text="Return to Feedback Page" OnClick="btnCustomerFeedback_Click" />
</asp:Content>
