﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;

/// <summary>
/// Contains the code for the ContactFeedback.aspx page
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class CustomerFeedback : Page
{
    /// <summary>
    /// Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;
        this.GenerateDescription();
        Response.Redirect("FeedbackComplete.aspx");
    }
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SetFocus(this.tbxCustomerID);
        this.SetControls(false);
    }

    /// <summary>
    /// Handles the Click event of the btnSubmitCustomerID control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSubmitCustomerID_Click(object sender, EventArgs e)
    {
        this.ResetControls();
        this.LoadFeedback();
    }

    /// <summary>
    /// Loads the feedback.
    /// </summary>
    private void LoadFeedback()
    {
        this.lbxClosedFeedback.Items.Clear();
        var customerFeedback = new List<Feedback>();

        var feedbackView = (DataView)this.sdsFeedback.Select(DataSourceSelectArguments.Empty);
        if (feedbackView == null)
        {
            return;
        }
        feedbackView.RowFilter = string.Format("CustomerID = '{0}' AND DateClosed IS NOT NULL", this.tbxCustomerID.Text);
        System.Diagnostics.Debug.WriteLine("Found " + feedbackView.Count);

        if (feedbackView.Count == 0)
        {
            this.rfvCustomerID.IsValid = true;
            return;
        }

        for (var i = 0; i < feedbackView.Count; i++)
        {
            var row = feedbackView[i];
            var newFeedback = CreateFeedback(row);
            customerFeedback.Add(newFeedback);
        }

        foreach (var feedback in customerFeedback)
        {
            this.lbxClosedFeedback.Items.Add(feedback.FormatFeedback());
        }
        System.Diagnostics.Debug.WriteLine("List size " + customerFeedback.Count);
        this.SetControls(true);
        this.lbxClosedFeedback.SelectedIndex = 0;
        SetFocus(this.lbxClosedFeedback);
        Session["FeedbackList"] = customerFeedback;
    }

    /// <summary>
    /// Creates the feedback.
    /// </summary>
    /// <param name="row">The row.</param>
    /// <returns></returns>
    /// <exception cref="Exception">Row cannot be null</exception>
    private static Feedback CreateFeedback(DataRowView row)
    {
        if (row == null)
        {
            throw new Exception("Row cannot be null");
        }

        var newFeedback = new Feedback
        {
            FeedbackId = Convert.ToInt32(row["FeedbackID"].ToString()),
            CustomerId = Convert.ToInt32(row["CustomerID"].ToString()),
            SoftwareId = row["SoftwareID"].ToString(),
            SupportId = Convert.ToInt32(row["SupportID"].ToString()),
            DateOpened = row["DateOpened"].ToString(),
            DateClosed = row["DateClosed"].ToString(),
            Title = row["Title"].ToString(),
            Description = row["Description"].ToString()
        };

        return newFeedback;
    }

    /// <summary>
    /// Resets the controls.
    /// </summary>
    private void ResetControls()
    {
        if (this.rblEfficency.SelectedValue != null)
        {
            this.rblEfficency.ClearSelection();
        }
        if (this.rblResolution.SelectedValue != null)
        {
            this.rblResolution.ClearSelection();
        }
        if (this.rblService.SelectedValue != null)
        {
            this.rblService.ClearSelection();
        }
        if (this.rblContactMethod.SelectedValue != null)
        {
            this.rblContactMethod.ClearSelection();
        }
        this.SetControls(false);
    }

    /// <summary>
    /// Sets the controls.
    /// </summary>
    /// <param name="value">if set to <c>true</c> [value].</param>
    private void SetControls(bool value)
    {
        this.lbxClosedFeedback.Enabled = value;

        this.rblEfficency.Enabled = value;
        this.rblResolution.Enabled = value;
        this.rblService.Enabled = value;

        this.tbxAdditionalComments.Enabled = value;

        this.cbxContact.Enabled = value;
        this.rblContactMethod.Enabled = value;

        this.btnSubmit.Enabled = value;
    }


    /// <summary>
    /// Generates the description.
    /// </summary>
    private void GenerateDescription()
    {
        var customerFeedback = (List<Feedback>)Session["FeedbackList"];
        var index = this.lbxClosedFeedback.SelectedIndex;
        var feedback = customerFeedback[index];
        var description = new Description
        {
            CustomerId = feedback.CustomerId,
            FeedbackId = feedback.FeedbackId,
            ServiceTime = Convert.ToInt32(this.rblService.SelectedValue),
            Efficiency = Convert.ToInt32(this.rblEfficency.SelectedValue),
            Resolution = Convert.ToInt32(this.rblResolution.SelectedValue),
            Comments = this.tbxAdditionalComments.Text,
            Contact = this.cbxContact.Checked,
            ContactMethod = this.rblContactMethod.SelectedValue
        };

        Session["Description"] = description;
    }
}