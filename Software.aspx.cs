﻿using System;
/// <summary>
/// Codebehind for the products page
/// </summary>
public partial class Products : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblGridViewError.Text = "";
        }
    }

    /// <summary>
    /// Handles the RowUpdated event of the gvCategories control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvCategories_RowUpdated(object sender, System.Web.UI.WebControls.GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblGridViewError.Text = "A database error has occurred.<br /><br />" +
                e.Exception.Message;
            if (e.Exception.InnerException != null)
            {
                this.lblGridViewError.Text += "<br />Message: "
                    + e.Exception.InnerException.Message;
                this.lblGridViewError.Focus();
            }

            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblGridViewError.Text = "Another user may have updated that category."
                + "<br />Please try again.";
            this.lblGridViewError.Focus();
        }
    }

}