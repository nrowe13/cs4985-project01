﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for CustomerList
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
[Serializable]
public class CustomerList
{
    /// <summary>
    /// The _customer list
    /// </summary>
    private readonly List<Customer> _customerList;

    /// <summary>
    /// Initializes a new instance of the <see cref="CustomerList"/> class.
    /// </summary>
    public CustomerList()
    {
        this._customerList = new List<Customer>();
    }

    /// <summary>
    /// Gets or sets the Customer at a given index.
    /// </summary>
    /// <value>
    /// The <see cref="Customer"/>.
    /// </value>
    /// <param name="index">Index of the Customer.</param>
    /// <returns>
    /// The Customer located at the given index.
    /// </returns>
    public Customer this[int index]
    {
        get { return this._customerList[index]; }
        set { this._customerList.Insert(index, value); }
    }

    /// <summary>
    /// Gets a Customer based on a given name.
    /// </summary>
    /// <value>
    /// The <see cref="Customer"/>.
    /// </value>
    /// <param name="name">The name to search for.</param>
    /// <returns>
    /// Customer with a matching name.
    /// </returns>
    public Customer this[string name]
    {
        get { return this._customerList.Find(customer => customer.Name == name); }
    }

    /// <summary>
    /// Gets the number of Customers stored in the list of Customers.
    /// </summary>
    /// <returns>
    /// Number of stored Customers.
    /// </returns>
    public int Count()
    {
        return this._customerList.Count;
    }

    /// <summary>
    /// Adds a new Customer to the list of Customers.
    /// </summary>
    /// <param name="newCustomer">Customer to be added.</param>
    public void AddItem(Customer newCustomer)
    {
        this._customerList.Add(newCustomer);
    }

    /// <summary>
    /// Removes a Customer from the list at a given location.
    /// </summary>
    /// <param name="index">The location of the Customer to be removed.</param>
    public void RemoveAt(int index)
    {
        this._customerList.RemoveAt(index);
    }

    /// <summary>
    /// Emptys the list of all stored Customers.
    /// </summary>
    public void Clear()
    {
        this._customerList.Clear();
    }

    /// <summary>
    /// Gets the customers.
    /// </summary>
    /// <returns></returns>
    public static CustomerList GetCustomers()
    {
        if (HttpContext.Current.Session["CustomerList"] != null)
        {
            return (CustomerList)HttpContext.Current.Session["CustomerList"];
        }
        var newCustomerList = new CustomerList();
        HttpContext.Current.Session["CustomerList"] = (newCustomerList);
        return newCustomerList;
    }

}