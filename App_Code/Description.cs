﻿using System;

/// <summary>
/// Stores the a detailed description of a customer's feedback.
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
[Serializable]
public class Description
{

    /// <summary>
    /// The _comments
    /// </summary>
    private string _comments;
    /// <summary>
    /// The _contact method
    /// </summary>
    private string _contactMethod;

    /// <summary>
    /// Gets and sets the CustomerID.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public int CustomerId { get; set; }
    /// <summary>
    /// Gets and sets the FeedbackId.
    /// </summary>
    /// <value>
    /// The feedback identifier.
    /// </value>
    public int FeedbackId { get; set; }
    /// <summary>
    /// Gets and sets the ServiceTime.
    /// </summary>
    /// <value>
    /// The service time.
    /// </value>
    public int ServiceTime { get; set; }
    /// <summary>
    /// Gets and sets the Efficency.
    /// </summary>
    /// <value>
    /// The efficiency.
    /// </value>
    public int Efficiency { get; set; }
    /// <summary>
    /// Gets and sets the Resolution.
    /// </summary>
    /// <value>
    /// The resolution.
    /// </value>
    public int Resolution { get; set; }
    /// <summary>
    /// Gets and sets the Comments.
    /// </summary>
    /// <value>
    /// The comments.
    /// </value>
    /// <exception cref="System.ArgumentException">Comments cannot be null</exception>
    public string Comments
    {
        get { return this._comments; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Comments cannot be null");
            }
            this._comments = value;
        }
    }
    /// <summary>
    /// Gets and sets if the customer wants to be contacted.
    /// </summary>
    /// <value>
    ///   <c>true</c> if contact; otherwise, <c>false</c>.
    /// </value>
    public bool Contact { get; set; }
    /// <summary>
    /// Gets and sets the ContactMethod.
    /// </summary>
    /// <value>
    /// The contact method.
    /// </value>
    /// <exception cref="System.ArgumentException">ContactMethod cannot be null</exception>
    public string ContactMethod
    {
        get { return this._contactMethod; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("ContactMethod cannot be null");
            }
            this._contactMethod = value;
        }
    }

}