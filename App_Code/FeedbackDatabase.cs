﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Summary description for FeedbackDatabase
/// </summary>
public static class FeedbackDatabase
{
    /// <summary>
    /// Gets the open feedback incidents.
    /// </summary>
    /// <param name="supportStaffId">The support staff identifier.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetOpenFeedbackIncidents(int supportStaffId)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const String selection = "SELECT SupportID, DateOpened, SoftwareID, CustomerID FROM Feedback " +
            "WHERE SupportID = @SupportID";
        var command = new OleDbCommand(selection, connection);
        command.Parameters.AddWithValue("SupportID", supportStaffId);
        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    /// Gets the customer feedback.
    /// </summary>
    /// <param name="customerId">The customer identifier.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetCustomerFeedback(int customerId)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const String selection = "SELECT SupportID, SoftwareID, DateOpened, DateClosed, Title, Description FROM Feedback " +
            "WHERE CustomerID = @CustomerID";
        var command = new OleDbCommand(selection, connection);
        command.Parameters.AddWithValue("CustomerID", customerId);
        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    /// Updates the feedback.
    /// </summary>
    /// <param name="originalFeedback">The original feedback.</param>
    /// <param name="newFeedback">The new feedback.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int UpdateFeedback(Feedback originalFeedback, Feedback newFeedback)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const String selection = "SELECT SupportID, DateOpened, SoftwareID, CustomerID FROM Feedback " +
            "WHERE SupportID = @SupportID";
        var command = new OleDbCommand(selection, connection);
        command.Parameters.AddWithValue("SupportID", newFeedback);
        connection.Open();
        return 0;
    }

}