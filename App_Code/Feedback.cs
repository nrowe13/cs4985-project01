﻿using System;

/// <summary>
/// Stores data for a customer's feedback.
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
[Serializable]
public class Feedback
{

    /// <summary>
    /// The _software identifier
    /// </summary>
    private string _softwareId;
    /// <summary>
    /// The _date opened
    /// </summary>
    private string _dateOpened;
    /// <summary>
    /// The _date closed
    /// </summary>
    private string _dateClosed;
    /// <summary>
    /// The _title
    /// </summary>
    private string _title;
    /// <summary>
    /// The _description
    /// </summary>
    private string _description;

    /// <summary>
    /// Gets and sets the FeedbackID.
    /// </summary>
    /// <value>
    /// The feedback identifier.
    /// </value>
    public int FeedbackId { get; set; }
    /// <summary>
    /// Gets and sets the CustomerID.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public int CustomerId { get; set; }
    /// <summary>
    /// Gets and sets the SoftwareID.
    /// </summary>
    /// <value>
    /// The software identifier.
    /// </value>
    /// <exception cref="System.ArgumentException">SoftwareId cannot be null</exception>
    public string SoftwareId
    {
        get
        {
            return this._softwareId;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("SoftwareId cannot be null");
            }
            this._softwareId = value;
        }
    }
    /// <summary>
    /// Gets and sets the SupportID.
    /// </summary>
    /// <value>
    /// The support identifier.
    /// </value>
    public int SupportId { get; set; }
    /// <summary>
    /// Gets and sets the DateOpened.
    /// </summary>
    /// <value>
    /// The date opened.
    /// </value>
    /// <exception cref="System.ArgumentException">DateOpened cannot be null</exception>
    public string DateOpened
    {
        get
        {
            return this._dateOpened;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("DateOpened cannot be null");
            }
            this._dateOpened = value;
        }
    }
    /// <summary>
    /// Gets and sets the DateClosed.
    /// </summary>
    /// <value>
    /// The date closed.
    /// </value>
    /// <exception cref="System.ArgumentException">DateClosed cannot be null</exception>
    public string DateClosed
    {
        get
        {
            return this._dateClosed;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("DateClosed cannot be null");
            }
            this._dateClosed = value;
        }
    }
    /// <summary>
    /// Gets and sets the Title.
    /// </summary>
    /// <value>
    /// The title.
    /// </value>
    /// <exception cref="System.ArgumentException">Title cannot be null</exception>
    public string Title
    {
        get
        {
            return this._title;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Title cannot be null");
            }
            this._title = value;
        }
    }
    /// <summary>
    /// Gets and sets the Description.
    /// </summary>
    /// <value>
    /// The description.
    /// </value>
    /// <exception cref="System.ArgumentException">Description cannot be null</exception>
    public string Description
    {
        get
        {
            return this._description;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Description cannot be null");
            }
            this._description = value;
        }
    }

    /// <summary>
    /// Generates a formatted string version of the feedback.
    /// </summary>
    /// <returns>
    /// Formatted string of this feedback.
    /// </returns>
    public string FormatFeedback()
    {
        return ("Feedback for software " + this.SoftwareId + " closed " + this.DateClosed + " (" + this.Title + ")");
    }

}