﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.OleDb;

/// <summary>
/// CustomerDatabase for pulling customer data from the database.
/// </summary>
public static class CustomerDatabase
{

    /// <summary>
    /// Gets the customers with feedback.
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetCustomersWithFeedback()
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const String selection = "SELECT CustomerID, Name FROM Customer WHERE CustomerID IN (SELECT DISTINCT CustomerID FROM Feedback WHERE SupportID IS NOT NULL) ORDER By Name";
        var command = new OleDbCommand(selection, connection);
        connection.Open();
        var dataReader = command.ExecuteReader();
        return dataReader;
    }

}