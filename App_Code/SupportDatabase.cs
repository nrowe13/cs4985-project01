﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.OleDb;

/// <summary>
/// 
/// </summary>
public static class SupportDatabase
{
    /// <summary>
    /// Gets all support staff.
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable GetAllSupportStaff()
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const String selection = "SELECT SupportID, Name FROM Support ORDER BY Name";
        var command = new OleDbCommand(selection, connection);
        connection.Open();
        var dataReader = command.ExecuteReader();
        return dataReader;
    }

}