﻿using System;

/// <summary>
/// Represents a customer and the information pertaining to them.
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
[Serializable]
public class Customer
{

    /// <summary>
    /// The _name
    /// </summary>
    private string _name;
    /// <summary>
    /// The _address
    /// </summary>
    private string _address;
    /// <summary>
    /// The _city
    /// </summary>
    private string _city;
    /// <summary>
    /// The _state
    /// </summary>
    private string _state;
    /// <summary>
    /// The _zip code
    /// </summary>
    private string _zipCode;
    /// <summary>
    /// The _phone
    /// </summary>
    private string _phone;
    /// <summary>
    /// The _email
    /// </summary>
    private string _email;

    /// <summary>
    /// Gets or sets the CustomerId of the customer.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public int CustomerId { get; set; }

    /// <summary>
    /// Gets or sets the Name of the customer.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    /// <exception cref="ArgumentException">Name cannot be null</exception>
    public string Name
    {
        get { return this._name; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Name cannot be null");
            }
            this._name = value;
        }
    }

    /// <summary>
    /// Gets or sets the Address of the customer.
    /// </summary>
    /// <value>
    /// The address.
    /// </value>
    /// <exception cref="ArgumentException">Address cannot be null</exception>
    public string Address
    {
        get { return this._address; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Address cannot be null");
            }
            this._address = value;
        }
    }

    /// <summary>
    /// Gets or sets the City of the customer.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    /// <exception cref="ArgumentException">City cannot be null</exception>
    public string City
    {
        get { return this._city; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("City cannot be null");
            }
            this._city = value;
        }
    }

    /// <summary>
    /// Gets or sets the State of the customer.
    /// </summary>
    /// <value>
    /// The state.
    /// </value>
    /// <exception cref="ArgumentException">City cannot be null</exception>
    public string State
    {
        get { return this._state; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("City cannot be null");
            }
            this._state = value;
        }
    }

    /// <summary>
    /// Gets or sets the ZipCode of the customer.
    /// </summary>
    /// <value>
    /// The zip code.
    /// </value>
    /// <exception cref="ArgumentException">ZipCode cannot be null</exception>
    public string ZipCode
    {
        get { return this._zipCode; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("ZipCode cannot be null");
            }
            this._zipCode = value;
        }
    }

    /// <summary>
    /// Gets or sets the Phone number of the customer.
    /// </summary>
    /// <value>
    /// The phone.
    /// </value>
    /// <exception cref="ArgumentException">Phone cannot be null</exception>
    public string Phone
    {
        get { return this._phone; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Phone cannot be null");
            }
            this._phone = value;
        }
    }

    /// <summary>
    /// Gets or sets the Email address of the customer.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    /// <exception cref="ArgumentNullException">value</exception>
    public string Email
    {
        get { return this._email; }
        set
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            this._email = value;
        }
    }

}