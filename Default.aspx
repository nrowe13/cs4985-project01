﻿<%@ Page AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" Language="C#" MasterPageFile="~/Site.master" Title="Home" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Welcome</h2>
    <br />
    <asp:Label ID="lblAbout" runat="server" Text="This is the Digital Upscale Manager for Ballgames database. For inquiries concerning customer contact information, please click Customer Display. For customer feedback inquiries, please click Customer Feedback."></asp:Label>
    <br />
    <h2>Directory</h2>
    <br />
    <asp:Button ID="btnCustomerDisplay" runat="server" Text="Customer Display" OnClick="btnCustomerDisplay_Click" />
    <asp:Button ID="btnCustomerFeedback" runat="server" Text="Customer Feedback" OnClick="btnCustomerFeedback_Click" />
    <br />
</asp:Content>
