﻿<%@ Page Title="Customer Feedback" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Customer Feedback</h2>
    <br />
    <asp:RequiredFieldValidator ID="rfvCustomerID" runat="server" ControlToValidate="tbxCustomerID" Display="Dynamic" ErrorMessage="You must enter a valid Customer ID" CssClass="validator" ValidationGroup="customerID" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvCustomerID" runat="server" ControlToValidate="tbxCustomerID" Display="Dynamic" ErrorMessage="Given Customer ID is invalid" CssClass="validator" Operator="DataTypeCheck" Type="Integer" ValidationGroup="customerID" SetFocusOnError="True"></asp:CompareValidator>
    <br />
    <asp:Label ID="lblCustomerId" runat="server" Text="Customer ID"></asp:Label>
    <asp:TextBox ID="tbxCustomerID" runat="server"></asp:TextBox>
    <asp:Button ID="btnSubmitCustomerID" runat="server" Text="Submit" OnClick="btnSubmitCustomerID_Click" ValidationGroup="customerID" />
    <br />
    <br />
    <asp:RequiredFieldValidator ID="rfvClosedFeedback" runat="server" ControlToValidate="lbxClosedFeedback" CssClass="validator" Display="Dynamic" ErrorMessage="You must select a feedback from the list" SetFocusOnError="True" ValidationGroup="listbox"></asp:RequiredFieldValidator>
    <br />
    <div class="listboxDiv">
        <asp:ListBox ID="lbxClosedFeedback" runat="server" Height="200px" Width="550px" Style="overflow-x: auto;" Enabled="False" ValidationGroup="listbox"></asp:ListBox>
    </div>

    <br />
    <asp:SqlDataSource ID="sdsFeedback" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [FeedbackID], [CustomerID], [SoftwareID], [SupportID], [DateOpened], [DateClosed], [Title], [Description] FROM [Feedback]"></asp:SqlDataSource>
    <br />
    <asp:Label ID="lblServiceTime" runat="server" Text="Service Time"></asp:Label>
    <asp:RequiredFieldValidator ID="rfvService" runat="server" ControlToValidate="rblService" Display="Dynamic" CssClass="validator">*</asp:RequiredFieldValidator>
    <br />
    <asp:RadioButtonList ID="rblService" runat="server">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied or Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <br />
    <asp:Label ID="lblEfficency" runat="server" Text="Technical Efficiency"></asp:Label>
    <asp:RequiredFieldValidator ID="rfvEfficency" runat="server" ControlToValidate="rblEfficency" Display="Dynamic" CssClass="validator">*</asp:RequiredFieldValidator>
    <br />
    <asp:RadioButtonList ID="rblEfficency" runat="server">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied or Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <br />
    <asp:Label ID="lblResolution" runat="server" Text="Problem Resolution"></asp:Label>
    <asp:RequiredFieldValidator ID="rfvResolution" runat="server" ControlToValidate="rblResolution" Display="Dynamic" CssClass="validator">*</asp:RequiredFieldValidator>
    <br />
    <asp:RadioButtonList ID="rblResolution" runat="server">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied or Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <br />
    <asp:Label ID="lblAdditionalComments" runat="server" Text="Additional Comments"></asp:Label>
    <br />
    <asp:TextBox ID="tbxAdditionalComments" runat="server" Height="125px" TextMode="MultiLine" Width="400px"></asp:TextBox>
    <br />
    <br />
    <asp:CheckBox ID="cbxContact" runat="server" Text="I wish to be contacted" />
    <asp:RequiredFieldValidator ID="rfvContact" runat="server" ControlToValidate="rblContactMethod" Display="Dynamic" CssClass="validator">*</asp:RequiredFieldValidator>
    <br />
    <asp:RadioButtonList ID="rblContactMethod" runat="server">
        <asp:ListItem Value="Email">Email</asp:ListItem>
        <asp:ListItem Value="Phone">Phone</asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <br />
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" EnableViewState="False" Width="61px" />
    <br />
</asp:Content>
