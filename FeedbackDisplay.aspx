﻿<%@ Page Title="Incident Display" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="FeedbackDisplay.aspx.cs" Inherits="IncidentDisplay" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Feedback completed</h2>
    <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" DataSourceID="sdsCustomers" DataTextField="Name" DataValueField="CustomerID">
    </asp:DropDownList>
    <asp:SqlDataSource ID="sdsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [CustomerID], [Name] FROM [Customer]"></asp:SqlDataSource>
    <br />
    <br />
    <asp:DataList ID="dlIncidents" runat="server" DataKeyField="FeedbackID" DataSourceID="sdsProducts" CellPadding="4" ForeColor="#333333">
        <AlternatingItemStyle BackColor="White" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderTemplate>
            <table class="header">
                <tr>
                    <td class="col1">Software/Incident</td>
                    <td class="col2">Technician Name</td>
                    <td class="col3">Date Opened</td>
                    <td class="col4">Date Closed</td>
                </tr>
            </table>
        </HeaderTemplate>
        <ItemStyle BackColor="#EFF3FB" />
        <ItemTemplate>
            <table>
                <tr>
                    <td class="col1">
                        <asp:Label ID="lblSoftwareID" runat="server" Text='<%# Eval("SoftwareID") %>' />
                    </td>
                    <td class="col2">
                        <asp:Label ID="lblSupportID" runat="server" Text='<%# Eval("FeedbackID") %>' />
                    </td>
                    <td class="col3">
                        <asp:Label ID="lblDateOpened" runat="server" Text='<%# Eval("DateOpened") %>' />
                    </td>
                    <td class="col4">
                        <asp:Label ID="lblDateClosed" runat="server" Text='<%# Eval("DateClosed") %>' />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="col5">
                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    </asp:DataList>
    <asp:SqlDataSource ID="sdsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT SoftwareID, FeedbackID, DateOpened, DateClosed, Description FROM Feedback WHERE (CustomerID = ?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlCategory" Name="CategoryID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>
