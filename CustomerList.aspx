﻿<%@ Page Title="Customer Selection" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="CustomerSelection" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Customer Selection</h2>
    <asp:DropDownList ID="ddlCustomers" runat="server" AutoPostBack="True" DataSourceID="sdsCustomers" DataTextField="Name" DataValueField="Name" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" AppendDataBoundItems="true">
        <asp:ListItem Selected="True" Text="--Select a Customer--" Value=""></asp:ListItem>
    </asp:DropDownList>

    <asp:SqlDataSource ID="sdsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Name], [CustomerID], [Address], [State], [ZipCode], [City], [Phone], [Email] FROM [Customer]"></asp:SqlDataSource>
    <br />
    <br />
    <div class="contactInfoDiv">
        <asp:Label ID="lblName" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblPhone" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblEmail" runat="server"></asp:Label>
    </div>
    <br />
    <h2>Customer Options</h2>
    <asp:Button ID="btnContactList" runat="server" Text="View Contact List" OnClick="btnContactList_Click" />
    <asp:Button ID="btnAddContacts" runat="server" Text="Add to Contacts" OnClick="btnAddContacts_Click" />
</asp:Content>
