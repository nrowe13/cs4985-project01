﻿<%@ Page Title="Products" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="Software.aspx.cs" Inherits="Products" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Products</h2>
    <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="False" DataKeyNames="SoftwareID" DataSourceID="sdsProducts" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowUpdated="gvCategories_RowUpdated">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="SoftwareID" HeaderText="SoftwareID" ReadOnly="True" SortExpression="SoftwareID">
                <HeaderStyle Width="75px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxName" runat="server" Text='<%# Bind("Name") %>' CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="edit" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Version" SortExpression="Version">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxVersion" runat="server" Text='<%# Bind("Version") %>' CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvVersion" runat="server" ErrorMessage="Please enter a valid software version." ControlToValidate="tbxVersion" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="edit" />
                    <asp:CompareValidator ID="cvVersion" runat="server" ErrorMessage="Version must be in decimal format" Text="*" Display="Dynamic" CssClass="validator" Type="Double" ControlToValidate="tbxVersion" Operator="DataTypeCheck" ValidationGroup="edit"></asp:CompareValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("Version") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ReleaseDate" SortExpression="ReleaseDate">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>' CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReleaseDate" runat="server" ErrorMessage="Please enter a valid date." ControlToValidate="tbxReleaseDate" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="edit" />
                    <asp:CompareValidator ID="cvReleaseDate" runat="server" ErrorMessage="Date must be in MM/DD/YYYY format" Text="*" Display="Dynamic" CssClass="validator" Type="Date" ControlToValidate="tbxReleaseDate" Operator="DataTypeCheck" ValidationGroup="edit"></asp:CompareValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="sdsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Software]" DeleteCommand="DELETE FROM [Software] WHERE [SoftwareID] = ?" InsertCommand="INSERT INTO [Software] ([SoftwareID], [Name], [Version], [ReleaseDate]) VALUES (?, ?, ?, ?)" UpdateCommand="UPDATE [Software] SET [Name] = ?, [Version] = ?, [ReleaseDate] = ? WHERE [SoftwareID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="SoftwareID" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SoftwareID" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Version" Type="Decimal" />
            <asp:Parameter Name="ReleaseDate" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Version" Type="Decimal" />
            <asp:Parameter Name="ReleaseDate" Type="DateTime" />
            <asp:Parameter Name="SoftwareID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <asp:Label ID="lblGridViewError" runat="server" CssClass="validator"></asp:Label>
    <asp:ValidationSummary ID="vsProducts" runat="server" CssClass="validator" ValidationGroup="edit" />
    <br />
    <br />
    <asp:DetailsView ID="dvCategories" runat="server" Height="50px" Width="250px" AutoGenerateRows="False" CellPadding="4" DataKeyNames="CategoryID" DataSourceID="sdsProducts" DefaultMode="Insert" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:TemplateField HeaderText="SoftwareID" SortExpression="SoftwareID">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxSoftwareID" runat="server" Text='<%# Bind("SoftwareID") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tbxInsertSoftwareID" runat="server" Text='<%# Bind("SoftwareID") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSoftwareID" runat="server" ErrorMessage="Please enter a valid software id" ControlToValidate="tbxInsertSoftwareID" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="insert"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSoftwareID" runat="server" Text='<%# Bind("SoftwareID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tbxInsertName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertName" runat="server" ErrorMessage="Please enter a valid name" ControlToValidate="tbxInsertName" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="insert"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Version" SortExpression="Version">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxVersion" runat="server" Text='<%# Bind("Version") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tbxInsertVersion" runat="server" Text='<%# Bind("Version") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertVersion" runat="server" ErrorMessage="Please enter a valid version" ControlToValidate="tbxInsertVersion" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="insert"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvInsetVersion" runat="server" ErrorMessage="Version must be in decimal format" Text="*" Display="Dynamic" CssClass="validator" Type="Double" ControlToValidate="tbxInsertVersion" Operator="DataTypeCheck" ValidationGroup="insert"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("Version") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Release Date" SortExpression="Release Date">
                <EditItemTemplate>
                    <asp:TextBox ID="tbxReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="tbxInsertReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertReleaseDate" runat="server" ErrorMessage="Please enter a valid date" ControlToValidate="tbxInsertReleaseDate" Display="Dynamic" CssClass="validator" Text="*" ValidationGroup="insert"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvInsetReleaseDate" runat="server" ErrorMessage="Date must be in MM/DD/YYYY format" Text="*" Display="Dynamic" CssClass="validator" Type="Date" ControlToValidate="tbxInsertReleaseDate" Operator="DataTypeCheck" ValidationGroup="insert"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" ValidationGroup="insert" />
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>

    <asp:ValidationSummary ID="vsDataViewCategories" runat="server" CssClass="validator" ValidationGroup="insert" />

</asp:Content>
