﻿<%@ Page Title="Contact List" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactList" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Contact List</h2>
    <asp:ListBox ID="lbxCustomerList" runat="server" Height="150px" Width="500px"></asp:ListBox>
    <br />
    <br />
    <asp:Button ID="btnAdd" runat="server" Text="Select Additional Customers" OnClick="btnAdd_Click" />
    <br />
    <h2>Contact Removal</h2>
    <asp:Button ID="btnRemove" runat="server" Text="Remove Customer" OnClick="btnRemove_Click" />
    <asp:Button ID="btnClear" runat="server" Text="Clear List" OnClick="btnClear_Click" />
</asp:Content>
