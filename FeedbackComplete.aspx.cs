﻿using System;
using System.Web.UI;

/// <summary>
/// The code for the FeedbackComplete.aspx page
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class FeedbackComplete : Page
{

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Description"] == null)
        {
            return;
        }
        var description = (Description)Session["Description"];
        if (description.Contact)
        {
            this.lblContactResponse.Text = "Your request to be contacted via " + description.ContactMethod +
                                           " has been recieved. A company representative will contact you shortly.";
        }
    }

    /// <summary>
    /// Handles the Click event of the btnCustomerFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnCustomerFeedback_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerFeedback.aspx");
    }

}