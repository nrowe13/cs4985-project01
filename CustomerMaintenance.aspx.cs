﻿using System;
/// <summary>
/// Codebehind for CustomerMaintenance.aspx
/// </summary>
public partial class CustomerMaintenance : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblGridViewError.Text = "";
        }
        else
        {
            this.gvCustomers.SelectedIndex = 0;
        }
    }

    /// <summary>
    /// Handles the ItemUpdated event of the dvCustomerDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.DetailsViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerDetails_ItemUpdated(object sender, System.Web.UI.WebControls.DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblGridViewError.Text = "A database error has occurred.<br /><br />" +
                "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
            this.lblGridViewError.Text = "Another user may have updated that customer."
                + "<br />Please try again.";
        else
            this.gvCustomers.DataBind();
    }
    /// <summary>
    /// Handles the ItemInserted event of the dvCustomerDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.DetailsViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerDetails_ItemInserted(object sender, System.Web.UI.WebControls.DetailsViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblGridViewError.Text = "A database error has occurred.<br /><br />" +
                "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
        }
        else
            this.gvCustomers.DataBind();
    }
    /// <summary>
    /// Handles the ItemDeleted event of the dvCustomerDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.DetailsViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerDetails_ItemDeleted(object sender, System.Web.UI.WebControls.DetailsViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblGridViewError.Text = "A database error has occurred.<br /><br />" +
                "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblGridViewError.Text = "Another user may have updated that customer."
                                         + "<br />Please try again.";
        }
        else
        {
            this.gvCustomers.SelectedIndex -= 1;
            this.gvCustomers.DataBind();
        }
    }
}