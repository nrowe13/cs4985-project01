﻿using System;
using System.Data;
using System.Web.UI;

/// <summary>
/// Contains the code for the CustomerSelection.aspx page
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class CustomerSelection : Page
{

    /// <summary>
    /// Handles the Click event of the btnAddContacts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAddContacts_Click(object sender, EventArgs e)
    {
        if (this.ddlCustomers.SelectedValue.Equals(""))
        {
            return;
        }
        var customerList = CustomerList.GetCustomers();
        var newCustomer = this.GenerateSelectedCustomer(this.ddlCustomers.SelectedValue);

        if (customerList[newCustomer.Name] != null)
        {
            return;
        }
        customerList.AddItem(newCustomer);

        Session["CustomerList"] = customerList;

        Response.Redirect("ContactList.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnContactList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnContactList_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ddlCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlCustomers.SelectedValue.Equals(""))
        {
            this.ClearCustomerInfo();
            return;
        }
        this.SetCustomerInfo(this.GenerateSelectedCustomer(this.ddlCustomers.SelectedValue));
    }

    /// <summary>
    /// Sets the customer information.
    /// </summary>
    /// <param name="newCustomer">The new customer.</param>
    private void SetCustomerInfo(Customer newCustomer)
    {
        this.lblName.Text = newCustomer.Name;
        this.lblPhone.Text = newCustomer.Phone;
        this.lblEmail.Text = newCustomer.Email;
    }

    /// <summary>
    /// Clears the customer information.
    /// </summary>
    private void ClearCustomerInfo()
    {
        this.lblName.Text = "";
        this.lblPhone.Text = "";
        this.lblEmail.Text = "";
    }

    /// <summary>
    /// Generates the selected customer.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    /// <exception cref="Exception">Value cannot be null</exception>
    private Customer GenerateSelectedCustomer(string value)
    {
        if (value == null)
        {
            throw new Exception("Value cannot be null");
        }
        var customerDataView = (DataView)this.sdsCustomers.Select(DataSourceSelectArguments.Empty);
        if (customerDataView == null)
        {
            return new Customer();
        }
        customerDataView.RowFilter = string.Format("Name = '{0}'", value);
        var row = customerDataView[0];

        var newCustomer = new Customer
        {
            CustomerId = Convert.ToInt32(row["CustomerID"].ToString()),
            Name = row["Name"].ToString(),
            Address = row["Address"].ToString(),
            City = row["City"].ToString(),
            State = row["State"].ToString(),
            ZipCode = row["ZipCode"].ToString(),
            Phone = row["Phone"].ToString(),
            Email = row["Email"].ToString()
        };
        return newCustomer;
    }

}