﻿using System;

/// <summary>
/// Backend class for FeedbackBySupport page
/// </summary>
public partial class FeedbackBySupport : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e) { }

}