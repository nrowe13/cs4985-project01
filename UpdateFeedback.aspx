﻿<%@ Page Title="Update Feedback" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="UpdateFeedback.aspx.cs" Inherits="UpdateFeedback" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Update Feedback</h2>
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1"></asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCustomersWithFeedback" TypeName="CustomerDatabase"></asp:ObjectDataSource>
    <br />
    <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetCustomerFeedback" TypeName="FeedbackDatabase">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="CustomerId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
