﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// A ContactList used for storing Contact data.
/// </summary>
/// <author>
/// Nicholas Rowe
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class ContactList : System.Web.UI.Page
{

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.LoadContacts();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerList.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnRemove control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        if (this.lbxCustomerList.Items.Count <= 0 || this.lbxCustomerList.SelectedIndex == -1)
        {
            return;
        }
        var index = this.lbxCustomerList.SelectedIndex;
        this.lbxCustomerList.Items.RemoveAt(index);

        var storedCustomerList = CustomerList.GetCustomers();
        storedCustomerList.RemoveAt(index);
        Session["CustomerList"] = storedCustomerList;
    }

    /// <summary>
    /// Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        if (this.lbxCustomerList.Items.Count == 0)
        {
            return;
        }
        var storedCustomerList = CustomerList.GetCustomers();
        storedCustomerList.Clear();
        Session["CustomerList"] = storedCustomerList;
        this.lbxCustomerList.Items.Clear();
    }

    /// <summary>
    /// Loads the contacts.
    /// </summary>
    private void LoadContacts()
    {
        if (Session["CustomerList"] == null)
        {
            return;
        }
        this.lbxCustomerList.Items.Clear();

        var storedCustomerList = CustomerList.GetCustomers();
        var contacts = new List<Customer>();
        for (var i = 0; i < storedCustomerList.Count(); i++)
        {
            contacts.Add(storedCustomerList[i]);
        }

        var sortedContacts = contacts.OrderBy(customer => customer.Name).ToList();

        foreach (var contact in sortedContacts)
        {
            var customerInfo = (contact.Name + ": " + contact.Phone + "; " + contact.Email);
            this.lbxCustomerList.Items.Add(customerInfo);
        }
    }

}