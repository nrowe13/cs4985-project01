﻿<%@ Page Title="Contact Us" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Contact Us</h2>
    <br />
    <asp:Label ID="lblPhone" runat="server" Text="Call us at 88‐BallGame"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblHours" runat="server" Text="Open Monday to Friday: 9am to 5pm" />
    <br />
    <br />
    <asp:Label ID="lblEmail" runat="server" Text="Email us at "></asp:Label>
    <asp:HyperLink ID="hlEmail" runat="server" Text="info@ballgame.com" NavigateUrl="mailto:info@ballgame.com" />
    <br />
    <br />
    <asp:Label ID="lblAddressLineOne" runat="server" Text="Digital Upscale Manager for Ballgames"></asp:Label>
    <br />
    <asp:Label ID="lblAddressLineTwo" runat="server" Text="128 Ball Park Road"></asp:Label>
    <br />
    <asp:Label ID="lblAddressLineThree" runat="server" Text="Nowhere, TX 30147"></asp:Label>
    <br />
</asp:Content>
