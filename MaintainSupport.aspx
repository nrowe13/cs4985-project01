﻿<%@ Page Title="Maintain Support" MasterPageFile="~/Site.master" Language="C#" AutoEventWireup="true" CodeFile="MaintainSupport.aspx.cs" Inherits="MaintainSupport" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="Head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="Content">
    <h2>Maintain Support</h2>
    <asp:DropDownList ID="ddlSupport" runat="server"
        DataSourceID="sdsSupport" DataTextField="Name"
        DataValueField="SupportID" AutoPostBack="True">
    </asp:DropDownList>
    <asp:SqlDataSource ID="sdsSupport" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>"
        ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>"
        SelectCommand="SELECT [Name], [SupportID] FROM [Support] ORDER BY [Name]"></asp:SqlDataSource>
    <br />
    <br />
    <asp:FormView ID="fvSupportDetails" runat="server" DataSourceID="sdsSupportDetails"
        DataKeyNames="SupportID">
        <EditItemTemplate>
            SupportID:
            <asp:Label ID="SupportIDLabel1" runat="server"
                Text='<%# Eval("SupportID") %>' />
            <br />
            Name:
            <asp:TextBox ID="tbxEditName" runat="server" Text='<%# Bind("Name") %>' />
            <asp:RequiredFieldValidator ID="rfvEditName" runat="server" ErrorMessage="Please enter a valid name" ControlToValidate="tbxEditName" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            Email:
            <asp:TextBox ID="tbxEditEmail" runat="server" Text='<%# Bind("Email") %>' />
            <asp:RequiredFieldValidator ID="rfvEditEmail" runat="server" ErrorMessage="Please enter a valid email" ControlToValidate="tbxEditEmail" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            Phone:
            <asp:TextBox ID="tbxEditPhone" runat="server" Text='<%# Bind("Phone") %>' />
            <asp:RequiredFieldValidator ID="rfvEditPhone" runat="server" ErrorMessage="Please enter a valid phone number" ControlToValidate="tbxEditPhone" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True"
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server"
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            Name:
            <asp:TextBox ID="tbxInsertName" runat="server" Text='<%# Bind("Name") %>' />
            <asp:RequiredFieldValidator ID="rfvInsertName" runat="server" ErrorMessage="Please enter a valid name" ControlToValidate="tbxInsertName" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            Email:
            <asp:TextBox ID="tbxInsertEmail" runat="server" Text='<%# Bind("Email") %>' />
            <asp:RequiredFieldValidator ID="rfvInsertEmail" runat="server" ErrorMessage="Please enter a valid email" ControlToValidate="tbxInsertEmail" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            Phone:
            <asp:TextBox ID="tbxInsertPhone" runat="server" Text='<%# Bind("Phone") %>' />
            <asp:RequiredFieldValidator ID="rfvInsertPhone" runat="server" ErrorMessage="Please enter a valid phone number" ControlToValidate="tbxInsertPhone" Display="Dynamic" CssClass="validator" Text="*"></asp:RequiredFieldValidator>
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True"
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server"
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            SupportID:
            <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>' />
            <br />
            Name:
            <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
            <br />
            Email:
            <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
            <br />
            Phone:
            <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False"
                CommandName="Edit" Text="Edit" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False"
                CommandName="Delete" Text="Delete" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False"
                CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="sdsSupportDetails" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>"
        SelectCommand="SELECT [SupportID], [Name], [Email], [Phone] FROM [Support] WHERE ([SupportID] = @SupportID)"
        DeleteCommand="DELETE FROM [Support] WHERE [SupportID] = ?"
        InsertCommand="INSERT INTO [Support] ([SupportID], [Name], [Email], [Phone]) VALUES (?, ?, ?, ?)"
        UpdateCommand="UPDATE [Support] SET [Name] = ?, [Email] = ?, [Phone] = ? WHERE [SupportID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="SupportID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SupportID" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="SupportID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter Name="SupportID" PropertyName="SelectedValue" ControlID="ddlSupport" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:ValidationSummary ID="vsSupport" runat="server" CssClass="validator" />
</asp:Content>
